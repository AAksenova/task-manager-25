package ru.t1.aksenova.tm.exception.field;

public final class DescriptionEmptyException extends AbstractFieldException {

    public DescriptionEmptyException() {
        super("Error! Description is empty...");
    }

}
