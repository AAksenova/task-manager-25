package ru.t1.aksenova.tm.command;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.api.model.ICommand;
import ru.t1.aksenova.tm.api.service.IServiceLocator;
import ru.t1.aksenova.tm.enumerated.Role;

@Getter
public abstract class AbstractCommand implements ICommand {

    @Nullable
    protected IServiceLocator serviceLocator;

    @Nullable
    public abstract String getArgument();

    @Nullable
    public abstract String getDescription();

    @Nullable
    public abstract String getName();

    public abstract void execute();

    @Nullable
    public abstract Role[] getRoles();

    @Nullable
    public IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    @Nullable
    public void setServiceLocator(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Nullable
    public String getUserId() {
        return serviceLocator.getAuthService().getUserId();
    }

    @NotNull
    public String toString() {
        @Nullable final String name = getName();
        @Nullable final String argument = getArgument();
        @Nullable final String description = getDescription();
        String result = "";
        if (name != null && !name.isEmpty()) result += name;
        if (argument != null && !argument.isEmpty()) result += ", " + argument;
        if (description != null && !description.isEmpty()) result += ": " + description;
        return result;

    }

}
