package ru.t1.aksenova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.model.Project;
import ru.t1.aksenova.tm.util.TerminalUtil;

public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-remove-by-id";

    @NotNull
    public static final String DESCRIPTION = "Remove project by id.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        @Nullable final String userId = getUserId();
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("[ENTER PROJECT ID:]");
        @Nullable final String id = TerminalUtil.nextLine();

        @NotNull final Project project = getProjectService().findOneById(userId, id);
        getProjectTaskService().removeTaskToProject(userId, project.getId());
    }

}
