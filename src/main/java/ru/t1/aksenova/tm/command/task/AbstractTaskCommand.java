package ru.t1.aksenova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.api.service.IProjectTaskService;
import ru.t1.aksenova.tm.api.service.ITaskService;
import ru.t1.aksenova.tm.command.AbstractCommand;
import ru.t1.aksenova.tm.enumerated.Role;
import ru.t1.aksenova.tm.enumerated.Status;
import ru.t1.aksenova.tm.model.Task;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    @NotNull
    protected ITaskService getTaskService() {
        return getServiceLocator().getTaskService();
    }

    @NotNull
    protected IProjectTaskService getProjectTaskService() {
        return getServiceLocator().getProjectTaskService();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    protected void showTask(final Task task) {
        if (task == null) return;
        System.out.println("TASK ID: " + task.getId());
        System.out.println("TASK NAME: " + task.getName());
        System.out.println("TASK DESCRIPTION: " + task.getDescription());
        System.out.println("TASK STATUS: " + Status.toName(task.getStatus()));
        System.out.println("TASK PROJECT ID: " + task.getProjectId());
    }

    protected void renderTasks(@Nullable final List<Task> tasks) {
        int index = 1;
        for (final Task task : tasks) {
            if (task == null) continue;
            System.out.println(index);
            showTask(task);
            index++;
            System.out.println("---------------------");
        }
    }

}
