package ru.t1.aksenova.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.api.service.ICommandService;
import ru.t1.aksenova.tm.api.service.IPropertyService;
import ru.t1.aksenova.tm.command.AbstractCommand;
import ru.t1.aksenova.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    @Nullable
    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

    @NotNull
    protected IPropertyService getPropertyService() {
        return getServiceLocator().getPropertyService();
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
